<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="th">
<head>
<title>Aimmura เอมมูร่า เซซามิน อาหารเสริมเพื่อสุขภาพ งานวิจัยมหาวิทยาลัยเชียงใหม่</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="อาหารเสริมเพื่อสุขภาพ,อาหารเสริม,งานวิจัยมหาวิทยาลัยเชียงใหม่,มะเร็ง,ข้อเสื่อม,sesamin,รักษาโรค,aimmura,Aimmurax,Aimmurao,Aimmurae,Aimmurav,เซซามิน,งาดำ,เอมมูร่า,เอมมูร่าเซซามิน,ไอยรา,ผลิตภัณฑ์ไอยรา">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- css -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    .map-responsive{
    overflow:hidden;
    padding-bottom:23%;
    position:relative;

}
.map-responsive iframe{
    left:0;
    top:0;
    height:250px;
    width:700px;
    position:absolute;
}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    width: 50px;
    height: 50px;
    border-radius: 50%; /* Rounded corners */
    opacity: 1.0;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #00e58b; /* Add a dark-grey background on hover */
    color: white; 
    opacity: 0.5;
    filter: alpha(opacity=100); 
}
</style>
</head>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน"><i class="fa fa-chevron-up"></i></button>
<?php
        session_start();
        include('setdatabase.php');
        
        $perpage = 8;
        if (isset($_GET['page'])) {
        $page = $_GET['page'];
        } else {
        $page = 1;
        }

        $start = ($page - 1) * $perpage;
?>
<div class="header-top-w3layouts" >
    <div class="container">
        <div class="col-md-6 logo-w3">
             <a href="index.php"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> gritanon </li>
                <li><a href="login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-bottom-w3ls" style="box-shadow: 0px 4px 7px -1px rgba(0,0,0,0.32)">
    <div class="container">
        <div class="col-md-7 navigation-agileits">
            <nav class="navbar navbar-default">
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav ">
                        <li class=" active"><a href="index.php" class="hyper "><span>หน้าหลัก</span></a></li> 
                        <li><a href="contact.html" class="hyper"><span>ติดต่อเรา</span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
                <script>
                $(document).ready(function(){
                    $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                            $(this).toggleClass('open');        
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                            $(this).toggleClass('open');       
                        }
                    );
                });
                </script>
        <div class="col-md-4 search-agileinfo">
            <form action="" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" aria-label="Left Align">
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
            </form>
        </div>
        <div class="col-md-1 cart-wthree">
            <div class="cart"> 
                 <form action="cart.php" method="post" class="last"> 
                    <input type="hidden" name="cmd" value="_cart" />
                    <input type="hidden" name="display" value="1" />
                    <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-plus" aria-hidden="true" style="color: #00e58b;"></i></button>
                </form> 
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- submenu -->
<style type="text/css">
.mini-submenu{
  display:none;  
  background-color: rgba(0, 0, 0, 0);  
  border: 1px solid rgba(0, 0, 0, 0.9);
  border-radius: 4px;
  padding: 9px;  
  /*position: relative;*/
  width: 42px;

}

.mini-submenu:hover{
  cursor: pointer;
}

.mini-submenu .icon-bar {
  border-radius: 1px;
  display: block;
  height: 2px;
  width: 22px;
  margin-top: 3px;
}

.mini-submenu .icon-bar {
  background-color: #000;
}

#slide-submenu{
  background: rgba(0, 0, 0, 0.45);
  display: inline-block;
  padding: 0 8px;
  border-radius: 4px;
  cursor: pointer;
}
</style>
<?php
    $strSQL="SELECT*FROM type_product ORDER BY typeID ASC";
    $objQuery=mysqli_query($mysqli,$strSQL);
    $Num_Rows=mysqli_num_rows($objQuery);
?>
<div class="content">
	<div class="container">
        <div class="col-md-3 ">
    <div class="row">
        <div class="col-sm-12  sidebar">
    <div class="mini-submenu">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </div>
    <div class="list-group">
        <span href="#" class="list-group-item active">
            Product categories
            <span class="pull-right" id="slide-submenu">
                <i class="fa fa-times"></i>
            </span>
        </span>
        <a href="women.php" class="list-group-item">
           <i class="fa fa-search"></i>&nbsp; สินค้าทั้งหมด
        </a>
<?php
    while($objResult = mysqli_fetch_assoc($objQuery))
  {
?>
        <form name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">
        <input type="hidden" value="<?php echo $objResult['type'];?>" id="txtKeyword" name="txtKeyword">
        <button type="submit" class="list-group-item">
            <i class="fa fa-search"></i>&nbsp; <?php echo $objResult["type"];?>
        </button>
        </form>
<?php
}
?>  
    </div>        
</div>
    </div>
        </div>
        <script type="text/javascript">
            $(function(){
            $('#slide-submenu').on('click',function() {                 
                $(this).closest('.list-group').fadeOut('slide',function(){
                    $('.mini-submenu').fadeIn();    
                });
                
              });

            $('.mini-submenu').on('click',function(){       
                $(this).next('.list-group').toggle('slide');
                $('.mini-submenu').hide();
            })
            })
        </script>
        <!-- submenu -->
<?php
    ini_set('display_errors', 1);
    error_reporting(~0);

    $strKeyword = null;

    if(isset($_POST["txtKeyword"]))
    {
        $strKeyword = $_POST["txtKeyword"];
    }
?>
<?php 
        $strSQL = "SELECT  * FROM product WHERE type LIKE '%".$strKeyword."%' limit {$start} , {$perpage}";
        $objQuery=mysqli_query($mysqli,$strSQL);
        $Num_Rows=mysqli_num_rows($objQuery);
?>
		<div class="col-md-8 col-sm-8 women-dresses" style="padding-top: 10px;">   
		<?php while ($objResult = mysqli_fetch_assoc($objQuery)) { ?>
		      <form action="order.php" method="post">
		            <div class="col-md-4 top-product-grids">
		            <a href="product.php?ProductID=<?php echo $objResult["ProductID"];?>" title="คลิ๊ก อ่านข้อมูลสินค้า">
		            <div class="product-img" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
		                 <img class="img-responsive" width="140" src="ViewImage.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
		                <div class="p-mask">
		                    <form action="#" method="post">
		                    <input type="hidden" name="txtQty" value="1" /> 
		                    <input type="hidden" name="txtProductID" value="<?php echo $objResult["ProductID"];?>" size="2">
		                    <button type="submit" class="w3ls-cart pw3ls-cart" title="คลิ๊ก ใส่ตะกร้า"><font color="#5e5f5f"><center><b>Add to cart / ใส่ตะกร้า </b></center></font></button>
		                    </form>
		                </div>
		            </div></a>
		            <h4> <a href="product.php?ProductID=<?php echo $objResult["ProductID"];?>" title="คลิ๊ก อ่านข้อมูลสินค้า"><?php echo $objResult["ProductName"];?></a></h4>
		            <h5><?php echo $objResult["Price"];?> บาท </h5>
		            <br><br>                    
		            </div>          
		      </form>
		<?php } ?>
		<div class="clearfix"> </div>
		<center>
	    <?php
	     $strSQL2 = "SELECT * FROM product ";
	     $objQuery2=mysqli_query($mysqli,$strSQL2);
	     $total_record = mysqli_num_rows($objQuery2);
	     $total_page = ceil($total_record / $perpage);
	     ?>
	    <nav>
	     <ul class="pagination">
	         <li>
	             <a href="index.php?page=1" aria-label="Previous">
	             <span aria-hidden="true">&laquo;</span>
	             </a>
	         </li>
	             <?php for($i=1;$i<=$total_page;$i++){ ?>
	             <li><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
	         <?php } ?>
	         <li>
	             <a href="index.php?page=<?php echo $total_page;?>" aria-label="Next">
	             <span aria-hidden="true">&raquo;</span>
	             </a>
	         </li>
	     </ul>
	     </nav>
	    </center>
		</div>
	</div>
</div>

<!-- newsletter -->
    <div class="newsletter" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
        <div class="container">
            <div class="col-md-6 w3agile_newsletter_left">
                <h3>Newsletter</h3>
                <p>รับข่าวสารข้อมูลผลิตภัณฑ์ใหม่จากเราได้ที่นี่ กรอกอีเมลของท่าน</p>
            </div>
            <div class="col-md-6 w3agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Subscribe" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //newsletter -->
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
            <a href="index.php"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236 (น้องเก็ท)</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
</body>
</html>