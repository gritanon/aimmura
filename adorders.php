<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin - &#3592;&#3633;&#3604;&#3585;&#3634;&#3619;&#3619;&#3657;&#3634;&#3609;&#3588;&#3657;&#3634;</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="tcal.css" />
<script type="text/javascript" src="tcal.js"></script>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  session_start();
  require_once("Connect.php");

  //*** Update Last Stay in Login System
  $strSQL = "UPDATE data_user SET LastUpdate = NOW() WHERE UserID = '".$_SESSION["UserID"]."' ";
  $objQuery=mysqli_query($mysqli,$strSQL);  

  //*** Get User Login
  $strSQL = "SELECT * FROM data_user WHERE UserID = '".$_SESSION['UserID']."' ";
  $objQuery=mysqli_query($mysqli,$strSQL);  
  $objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
  if($_SESSION['UserID'] ==""){
    header("location:index.php");
  }
?>
  <header class="main-header">
    <!-- Logo -->
    <a href="admin.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">GET</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Store</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/boxed-bg.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $objResult["Name"];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $objResult["Name"];?>
                  <small><?php echo $objResult["LastUpdate"];?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $objResult["Name"];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          <a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center><font color="white">MENU</font></center></li>
        <li >
          <a href="admin.php">
            <i class="fa fa-home" aria-hidden="true"></i> <span>&#3627;&#3609;&#3657;&#3634;&#3627;&#3621;&#3633;&#3585;</span>
          </a>
        </li>
        <li class="active">
          <a href="adorders.php">
            <i class="fa fa-list-alt" aria-hidden="true"></i> <span>&#3619;&#3634;&#3618;&#3585;&#3634;&#3619;&#3626;&#3633;&#3656;&#3591;&#3595;&#3639;&#3657;&#3629;&#3607;&#3633;&#3657;&#3591;&#3627;&#3617;&#3604;</span>
          </a>
        </li>
        <li>
          <a href="adtype.php">
            <i class="fa fa-server" aria-hidden="true"></i><span>&#3611;&#3619;&#3632;&#3648;&#3616;&#3607;&#3626;&#3636;&#3609;&#3588;&#3657;&#3634;</span>
          </a>
        </li>
        <li>
          <a href="adproducts.php">
            <i class="fa fa-shopping-bag" aria-hidden="true"></i> <span>&#3626;&#3636;&#3609;&#3588;&#3657;&#3634;</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      </h1>
      <ol class="breadcrumb">
        <li><a href="adorders.php"><i class="fa fa-list-alt" aria-hidden="true"></i> &#3619;&#3634;&#3618;&#3585;&#3634;&#3619;&#3626;&#3633;&#3656;&#3591;&#3595;&#3639;&#3657;&#3629;</a></li>
      </ol>
      <br>
        
<center><form action="adorders.php" method="get"><strong>From : <input type="text" name="d1" class="tcal form" value="<?php echo @$_GET['d1'] ?>" /> To: <input type="text"  name="d2" class="tcal form" value="<?php echo @$_GET['d2'] ?>" />
 <button class="btn btn-info" style="width: 123px; height:35px; margin-top:-8px;margin-left:8px;" type="submit"><i class="icon icon-search icon-large"></i> Search</button>
</strong></form>
<a href="report/report.pdf"><button  style="float:right;" class="btn btn-success btn-mini"> Print</button></a></center>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-body table-responsive">
              <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>&#3648;&#3621;&#3586;&#3651;&#3610;&#3626;&#3633;&#3656;&#3591;&#3595;&#3639;&#3657;&#3629;</th>
                  <th>&#3623;&#3633;&#3609;&#3607;&#3637;&#3656; / &#3648;&#3623;&#3621;&#3634;</th>
                  <th>&#3594;&#3639;&#3656;&#3629;</th>
                  <th>&#3648;&#3610;&#3629;&#3619;&#3660;&#3650;&#3607;&#3619;</th>
                  <th>&#3610;&#3633;&#3597;&#3594;&#3637;&#3594;&#3635;&#3619;&#3632;</th>
                  <th>&#3592;&#3635;&#3609;&#3623;&#3609;&#3648;&#3591;&#3636;&#3609;</th>
                  <th>&#3626;&#3606;&#3634;&#3609;&#3632;</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>  
<?php
    include('setdatabase2.php');
      if(@$_GET['d1'] =="" & @$_GET['d2']==""){
        $result = $db->prepare("SELECT * FROM orders");
      }else{
        $d1=$_GET['d1'];
        $d2=$_GET['d2'];
        $result = $db->prepare("SELECT * FROM orders WHERE OrderDate BETWEEN :a AND :b ");
        $result->bindParam(':a', $d1);
        $result->bindParam(':b', $d2);
        
      }
        $result->execute();
        for($i=0; $row = $result->fetch(); $i++){
      ?>
            <tr><h5>
                <td><?php echo $row["OrderID"];?></td>
                <td><?php echo $row["OrderDate"];?></td>
                <td><?php echo $row["Name"];?></td>
                <td><?php echo $row["Tel"];?></td>
                <td><?php echo $row["Cardname"];?></td>
                <td><?php echo $row["Total"];?> &#3610;&#3634;&#3607;</td>
                <td><span class="label label-danger"><?php echo $row["status"];?></span></td>
                <td><a href="detail.php?OrderID=<?php echo $row["OrderID"];?>">
                <button type="button" class="btn btn-primary btn-xs"> 
                &#3604;&#3641;&#3619;&#3634;&#3618;&#3621;&#3632;&#3648;&#3629;&#3637;&#3618;&#3604;</button></a></td>
            </h5></tr>
<?php
}
?>    
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<?php
  require('fpdf.php');

  define('FPDF_FONTPATH','font/');

  class PDF extends FPDF
  {
    function Header(){
      $this->AddFont('angsa','','angsa.php');
      $this->SetFont('angsa','',15);
      $this->Cell(0,10,iconv( 'UTF-8','TIS-620','Report Ordering'),0,0,"C");
      $this->Cell(0,0,iconv( 'UTF-8','TIS-620',' '.$this->PageNo()),0,1,"C");
      $this->Ln(13);
      $w=array(18,34,31,25,31,18,20);
      $header=array('NO.','Date / Time','Name','Tel','Account Name','Amount','Status');
          for($i=0;$i<count($header);$i++)
          $this->Cell($w[$i],7,iconv('UTF-8','TIS-620',$header[$i]),1,0,'C');
        $this->Ln();
    }
  function ImprovedTable($data){
  
  foreach ($data as $eachResult) 
  {
    $this->Cell(18,8,iconv('UTF-8','TIS-620',$eachResult["OrderID"]),1);
    $this->Cell(34,8,iconv('UTF-8','TIS-620',$eachResult["OrderDate"]),1);
    $this->Cell(31,8,iconv('UTF-8','TIS-620',$eachResult["Name"]),1);
    $this->Cell(25,8,iconv('UTF-8','TIS-620',$eachResult["Tel"]),1);
    $this->Cell(31,8,iconv('UTF-8','TIS-620',$eachResult["Cardname"]),1);
    $this->Cell(18,8,iconv('UTF-8','TIS-620',$eachResult["Total"]),1);
    $this->Cell(20,8,iconv('UTF-8','TIS-620',$eachResult["status"]),1);
    $this->Ln();
  }
  
}
    function Footer(){
      $this->AddFont('angsa','','angsa.php');
      $this->SetFont('angsa','',10);
      $this->SetY(-15);
      $this->Cell(0,0,iconv( 'UTF-8','TIS-620','Create date : '.date("Y-m-d")),0,1,"R");
    }
   
  }

  $pdf=new PDF();
  $pdf->SetMargins( 15,5,15 );
  $pdf->AddFont('angsa','','angsa.php');
  $pdf->SetFont('angsa','',15);
  $pdf->Ln(10);
  $strSQL = "SELECT * FROM orders WHERE status != '' order by OrderID ASC";
  $objQuery=mysqli_query($mysqli,$strSQL);
  $resultData = array();
  for ($i=0;$i<mysqli_num_rows($objQuery);$i++) {
  $result = mysqli_fetch_array($objQuery);
  $SumTotal+=str_replace(',', '', $result["Total"]); 
  array_push($resultData,$result);
  }
  $pdf->AddPage();
  $pdf->ImprovedTable($resultData);
  $pdf->Output("report/report.pdf","F");
?>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 15px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #1e282c; /* Set a background color */
    color: #b8c7ce; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
    width: 45px;
    opacity: 0.5;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #222d32; /* Add a dark-grey background on hover */
    color: white;  
    opacity: 1.0;
    filter: alpha(opacity=100);
}
</style>
<button onclick="topFunction()" id="myBtn"><i class="fa fa-chevron-up"></i></button>
<!-- DataTables -->
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>