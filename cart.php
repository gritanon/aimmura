<!DOCTYPE html>
<html lang="th">
<head>
<title>Meeyer.com อาหารเสริม</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/button-go-top.js"></script>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
   
}

th, td {
    border: 1px;
    padding: 3px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    width: 50px;
    height: 50px;
    border-radius: 50%; /* Rounded corners */
    opacity: 1.0;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #00e58b; /* Add a dark-grey background on hover */
    color: white; 
    opacity: 0.5;
    filter: alpha(opacity=100); 
}
</style>
</head>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน"><i class="fa fa-chevron-up"></i></button>
<?php
    session_start();
    include('setdatabase.php');

?>
<div class="header-top-w3layouts">
    <div class="container">
        <div class="col-md-6 logo-w3">
            <a href="index.php"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> gritanon </li>
                <li><a href="login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-bottom-w3ls">
    <div class="container">
        <div class="col-md-7 navigation-agileits">
            <nav class="navbar navbar-default">
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav ">
                        <li class=" active"><a href="index.php" class="hyper "><span>หน้าหลัก</span></a></li> 
                        <li><a href="contact.html" class="hyper"><span>ติดต่อเรา</span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
                                <script>
                $(document).ready(function(){
                    $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                            $(this).toggleClass('open');        
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                            $(this).toggleClass('open');       
                        }
                    );
                });
                </script>
        <div class="col-md-4 search-agileinfo">
            <form action="" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" >
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
            </form>
        </div>
        <div class="col-md-1 cart-wthree" >
            <div class="cart"> 
                 <form action="cart.php" method="post" class="last"> 
                    <input type="hidden" name="cmd" value="_cart" />
                    <input type="hidden" name="display" value="1" />
                    <button class="w3view-cart" type="submit" name="submit" value=""><i class="fa fa-cart-plus" aria-hidden="true" ></i></button>
                </form> 
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="container">
    <div class="col-md-1" ></div>
    <div class="col-md-7" >
        </br>
              <center><b>ตะกร้าสินค้า</b></center>
        </br>
          <form action="update.php" method="post">
            <div class="table-responsive" >
            <table border="1" align="center" width="100%" style="border-color:#ddd;border-style: solid;">
            <thead>
              <tr style="background-color: #f8f8f8">
                <td width="45"><center>สินค้า</center></td>
                <td><center>ชื่อ</center></td>
                <td style="border-left-style: hidden;border-right-style: hidden;"><center>ราคา/หน่วย</center></td>
                <td style="border-right-style: hidden;"><center>จำนวน</center></td>
                <td><center>ราคา</center></td>
                <td><center>ทิ้งสินค้า</center></td>
              </tr>
              </thead>
              <tbody>
              <?php
              $Total = 0;
              $SumTotal = 0;
              for($i=0;$i<=(int)$_SESSION["intLine"];$i++)
              {
                if($_SESSION["strProductID"][$i] != "")
                {
                $strSQL = "SELECT * FROM product WHERE ProductID = '".$_SESSION["strProductID"][$i]."' ";
                $objQuery = mysqli_query($mysqli,$strSQL);
                $objResult = mysqli_fetch_array($objQuery);
                $Total = $_SESSION["strQty"][$i] * $objResult["Price"];
                $SumTotal = $SumTotal + $Total;
                ?>
                <tr align="center" >
                <td><img class="img-responsive" width="140" src="ViewImage.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo"></td>
                <td style="border-top-style: hidden;"><?php echo $objResult["ProductName"];?></td>
                <td style="border-style: hidden;"><?php echo $objResult["Price"];?></td>
                <td style="border-style: hidden;"><input type="text" class="form-control" name="txtQty<?php echo $i;?>" value="<?php echo $_SESSION["strQty"][$i];?>" size="1"></td>
                <td style="border-top-style: hidden;"><?php echo number_format($Total,2);?></td>
                <td>
                <a href="delete.php?Line=<?php echo $i;?>" >
                    <span class="glyphicon glyphicon-remove" style="color: red;"></span>
                    </a></td>
                </tr>
                <?php
                }elseif($_SESSION['strProductID'] ==""){
                        echo "<script>alert('คุณไม่มีสินค้าอยู่ในตะกร้า');window.location ='index.php';</script>";
                }
              }
              ?>
              </tbody>
              <tfoot>
                <tr style="background-color: #f8f8f8">
                    <td colspan="6" style="text-align: right;">
                        จำนวนเงินรวมทั้งหมด <font size="5"><b><?php echo number_format($SumTotal,2);?> </b></font>บาท
                    </td>
                </tr>
                <tr style="background-color: white;">
                    <td align="center" ><a href="index.php" type="button" class="btn btn-primary">เลือกสินค้าเพิ่ม</a></td>
                    <td colspan="5" style="text-align: right;">
                        <button type="submit"  class="btn btn-info">คำนวณราคาสินค้าใหม่</button>
                        <a href="pay.php" type="button"  class="btn btn-primary">สังซื้อสินค้า</a>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
        </form>
        </div>
        <div class="col-md-3">
        <center><iframe  src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FMeeyerTH%2F&tabs=timeline&width=280&height=340&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="270" height="350" style="border:none;overflow:hidden;margin-top: 60px" scrolling="no" frameborder="0" allowTransparency="true"></iframe></center>
        </div>
         <div class="col-md-1"></div>
</div>
<div class="fandt">
    <div class="container">
        <div class="col-md-6 features">
            <h3>Our Services</h3>
            <div class="support">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-user " aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4><font size="3">ตอบกลับภายใน 24 ขั่วโมง</font></h4>
                    <p><a href="https://www.facebook.com/Meeyer-%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%B8%E0%B8%82%E0%B8%A0%E0%B8%B2%E0%B8%9E-1478566848879190/?modal=admin_todo_tour"> www.facebook.com/Meeyer</a>&nbsp;
                    <br><a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="shipping">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>ขนส่งสินค้าฟรี</h4>
                    <p>บริการขนส่งสินค้าฟรีทั่วทุกจังหวัด</p>
                </div>  
                <div class="clearfix"></div>
            </div>
            <div class="money-back">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>คืนเงินเมื่อเกิดข้อผิดพลาด</h4>
                    <p>เมื่อเกิดข้อผิดพลาดระหว่างการสั่งซื้อสินค้า ทางเราจะคืนเงินให้ท่าน 100%</p>
                </div>  
                <div class="clearfix"></div>                
            </div>
        </div>
        <div class="col-md-6 testimonials">
            <div class="test-inner">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
                <script src="js/jquery.wmuSlider.js"></script> 
                                <script>
                                    $('.example1').wmuSlider();         
                                </script> 
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
            <a href="index.php"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
</body>
</html>