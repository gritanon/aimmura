<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="th">
<head>
<title>Meeyer.com อาหารเสริม</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
<link rel="stylesheet" href="dist/css/thbanklogos.min.css" id="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="jquery.min.js"></script>
<script src="jquery.form.validator.min.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    .map-responsive{
    overflow:hidden;
    padding-bottom:23%;
    position:relative;

}
.map-responsive iframe{
    left:0;
    top:0;
    height:250px;
    width:700px;
    position:absolute;
}
</style>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
   
}

th, td {
    border: 1px;
    padding: 3px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 15px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
}

#myBtn:hover {
    background-color: #f8f8f8; /* Add a dark-grey background on hover */
    color: black;  
}
</style>
</head>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน">TOP</button>
<?php
    session_start();
    include('setdatabase.php');
    if($_SESSION['strProductID'] ==""){
        header("location:index.php");
      }
?>

<div class="header-top-w3layouts">
    <div class="container">
        <div class="col-md-6 logo-w3">
             <a href="index.php"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> gritanon </li>
                <li><a href="login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div style="overflow-x:auto;" >
<div style="background: #f8f8f8 ;text-align: center;">  
		<font size="3"><a href=""><b><span class="glyphicon glyphicon-ok-sign"></span> 1.กรอกที่อยู่ที่จะจัดส่ง</a></b></span> 
		<span class="glyphicon glyphicon-option-vertical"></span>
		<font color="gray">
        <span class="glyphicon glyphicon-remove-sign"></span> <b>2.ชำระเงิน</b></span></font></font>
</div>
</br>
<?php

              $Total = 0;
              $SumTotal = 0;
              for($i=0;$i<=(int)$_SESSION["intLine"];$i++)
              {
                if($_SESSION["strProductID"][$i] != "")
                {
                $strSQL = "SELECT * FROM product WHERE ProductID = '".$_SESSION["strProductID"][$i]."' ";
                $objQuery = mysqli_query($mysqli,$strSQL);
                $objResult = mysqli_fetch_array($objQuery);
                $Total = $_SESSION["strQty"][$i] * $objResult["Price"];
                $SumTotal = $SumTotal + $Total;
                ?>
<input type="hidden" name="txtQty<?php echo $i;?>" value="">
                <?php
                }
              }
              ?>
<div class="banner">
<div class="container">
<div class="col-md-7 " >
        <div class="container">
            <div class="row marketing"> 
                <div class="col-lg-8">  
                    <form class="form-horizontal" role="form" action="save_checkout.php" method="post" id="form">	
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-7">
                                <center><b>กรอกที่อยู่ที่จะจัดส่ง</b></center>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ชื่อ-นามสกุล</label>
                            <div class="col-sm-7">
                                <input type="text" name="txtName" placeholder="ชื่อ-นามสกุล" class="form-control"  data-validation="required" 
                                data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ที่อยู่</label>
                            <div class="col-sm-7">
                                <textarea class="form-control" name="txtAddress" 
                                data-validation="required"
                                data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้"
                                placeholder="ที่อยู่"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">จังหวัด</label>
                            <div class="col-sm-7">
                                <input type="text" name="province" placeholder="จังหวัด" class="form-control"  data-validation="required" data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้">  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">รหัสไปรษณีย์</label>
                            <div class="col-sm-7">
                                <input type="text" name="txtPost" placeholder="รหัสไปรษณีย์" class="form-control"  data-validation="required" 
                                data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">มือถือ</label>
                            <div class="col-sm-7">
                                <input type="text" name="txtTel" class="form-control" 
                                data-validation="required"
                                data-validation="custom" 
                                data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้"
                                placeholder="เบอร์โทรศัพท์มือถือที่ติดต่อได้">	
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ทางเลือกชำระเงิน</label>
                            <div class="col-sm-7">
                                 <img src="images/scb.jpg" style="width: 40px" /> SCB ไทยพาณิชย์ 996-258161-7 ออมทรัพย์ นาย กฤตานน เผ่าดี
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ชื่อบัญชีผู้ชำระเงิน</label>
                            <div class="col-sm-7">
                                <input type="text" name="Cardname" class="form-control" 
                                data-validation="required"
                                data-validation="custom" 
                                data-validation-error-msg="กรุณาใส่ข้อมูลช่องนี้"
                                placeholder="ชื่อบัญชีผู้ชำระเงิน">	
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10" >
                            	<input name="Total" type="hidden" id="id" value="<?php echo number_format($SumTotal,2);?>"/>
                                <button type="submit" class="btn btn-primary">สั่งซื้อ</button>
                                <button type="reset" class="btn" color="gray">ยกเลิก</button>
                            </div>
                        </div>
                     </form>
                </div>
            </div>
            </div>
    </div>
 <div class="col-md-5">
              <center><b>รายการสินค้า</b></center>
              <div class="table-responsive" style="margin-top: 14px;">
              <table border="1" align="center" width="100%" style="border-color:#ddd;border-style: solid;">
              <thead>
                <tr style="background-color: #f8f8f8">
                <th ><center>ชื่อสินค้า</center></th>
                <th style="border-left-style: hidden;border-right-style: hidden;"><center>ราคาต่อหน่วย</center></th>
                <th style="border-right-style: hidden;"><center>จำนวน</center></th>
                <th><center>รวมราคา</center></th>
                </tr>
              </thead>
              <tbody>
              <?php
              $Total = 0;
              $SumTotal = 0;
              for($i=0;$i<=(int)$_SESSION["intLine"];$i++)
              {
                if($_SESSION["strProductID"][$i] != "")
                {
                $strSQL = "SELECT * FROM product WHERE ProductID = '".$_SESSION["strProductID"][$i]."' ";
                $objQuery = mysqli_query($mysqli,$strSQL);
                $objResult = mysqli_fetch_array($objQuery);
                $Total = $_SESSION["strQty"][$i] * $objResult["Price"];
                $SumTotal = $SumTotal + $Total;
                ?>
                <tr align="center" >
                <td style="border-top-style: hidden;"><?php echo $objResult["ProductName"];?></td>
                <td style="border-style: hidden;"><?php echo $objResult["Price"];?></td>
                <td style="border-style: hidden;"><input type="hidden" name="txtQty<?php echo $i;?>" value=""><?php echo $_SESSION["strQty"][$i];?></td>
                <td style="border-top-style: hidden;"><?php echo number_format($Total,2);?></td>  
                </tr>
                <?php
                }
              }
              ?>
              </tbody>
              <tfoot>
                <tr style="background-color: #f8f8f8">
                    <td colspan="4" style="text-align: right;">
                        จำนวนเงินรวมทั้งหมด <font size="5"><b><?php echo number_format($SumTotal,2);?> </b></font>บาท &nbsp;
                    </td>
                </tr>
                </tfoot>
              </table>
        </div>
        </div>
    </div>
        <div class="clearfix"> </div>
</div>
        <script>
            $.validate({
                modules: 'security, file',
                onModulesLoaded: function () {
                    $('input[name="pass_confirmation"]').displayPasswordStrength();
                }
            });
        </script>
<div class="fandt" >
    <div class="container" >
        <div class="col-md-6 features">
            <h3>Our Services</h3>
            <div class="support">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-user " aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4><font size="3">ตอบกลับภายใน 24 ขั่วโมง</font></h4>
                    <p><a href="https://www.facebook.com/Meeyer-%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%B8%E0%B8%82%E0%B8%A0%E0%B8%B2%E0%B8%9E-1478566848879190/?modal=admin_todo_tour"> www.facebook.com/Meeyer</a>&nbsp;
                    <br><a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="shipping">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>ขนส่งสินค้าฟรี</h4>
                    <p>บริการขนส่งสินค้าฟรีทั่วทุกจังหวัด</p>
                </div>  
                <div class="clearfix"></div>
            </div>
            <div class="money-back">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>คืนเงินเมื่อเกิดข้อผิดพลาด</h4>
                    <p>เมื่อเกิดข้อผิดพลาดระหว่างการสั่งซื้อสินค้า ทางเราจะคืนเงินให้ท่าน 100%</p>
                </div>  
                <div class="clearfix"></div>                
            </div>
        </div>
        <div class="col-md-6 testimonials">
            <div class="test-inner">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
                <script src="js/jquery.wmuSlider.js"></script> 
                                <script>
                                    $('.example1').wmuSlider();         
                                </script> 
<!-- top-brands -->
    <div class="top-brands">
        <div class="container">
        </div>
    </div>
<!-- //top-brands -->
<!-- newsletter -->
    <div class="newsletter">
        <div class="container">
            <div class="col-md-6 w3agile_newsletter_left">
                <h3>Newsletter</h3>
                <p>รับข่าวสารข้อมูลผลิตภัณฑ์ใหม่จากเราได้ที่นี่ กรอกอีเมลของท่าน</p>
            </div>
            <div class="col-md-6 w3agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Subscribe" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //newsletter -->
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
                  <a href="index.php"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
</body>
</html>