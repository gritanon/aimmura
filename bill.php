 <!DOCTYPE html>
<html lang="th">
<head>
<title>Meeyer.com อาหารเสริม</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="jquery.min.js"></script>
        <script src="jquery.form.validator.min.js"></script>
        <script src="js/button-go-top.js"></script>
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
   
}

th, td {
    border: 1px;
    padding: 3px;
}

tr:nth-child(even){background-color: #f2f2f2}
</style>
<style>
    .map-responsive{
    overflow:hidden;
    padding-bottom:23%;
    position:relative;

}
.map-responsive iframe{
    left:0;
    top:0;
    height:250px;
    width:700px;
    position:absolute;
}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 15px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
}

#myBtn:hover {
    background-color: #f8f8f8; /* Add a dark-grey background on hover */
    color: black;  
}
</style>
</head>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน">TOP</button>
<?php
    session_start();
    include('setdatabase.php');
?>
<div class="header-top-w3layouts">
    <div class="container">
        <div class="col-md-6 logo-w3">
            <a href="index.php"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> Gritanon </li>
                <li><a href="login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div style="background: #f8f8f8 ;text-align: center;">  
		<font size="3"><a href=""><b><span class="glyphicon glyphicon-ok-sign"></span> 1.กรอกที่อยู่ที่จะจัดส่ง</a></b>
		<span class="glyphicon glyphicon-option-vertical"></span>
        <a href=""><span class="glyphicon glyphicon-ok-sign"></span> <b>2.ชำระเงิน</b></a></font></font>
</div>
</br>
<div class="banner">
<div class="container">
<div class="col-md-7 " >
    <?php
    $strSQL = "SELECT * FROM orders WHERE OrderID = '".$_GET["OrderID"]."' ";
    $objQuery=mysqli_query($mysqli,$strSQL);
    $objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
    ?>
    <div>
        <center><b>สถานที่จัดส่งสินค้า</b></center>
        <table class="table" align="center"  style="margin-top: 14px">
            <thead>
                <tr style="background-color: #f8f8f8">
                    <td >หมายเลขใบสั่งซื้อ <?php echo $objResult["OrderID"];?></td>
                    <td align="right">วันที่สั่งซื้อ <?php echo $objResult["OrderDate"];?></td>
                </tr>
            </thead>
            <tbody >
                <tr>
                    <td align="left" colspan="2">ชื่อ <?php echo $objResult["Name"];?> </br>เบอร์โทร <?php echo $objResult["Tel"];?></br>สถานที่จัดส่ง <?php echo $objResult["Address"];?> <?php echo $objResult["Province"];?> <?php echo $objResult["Post"];?></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>ชำระเงินด้วยบัญชีชื่อ <?php echo $objResult["Cardname"];?> </td>
                    <td align="right">ยอดสุทธิ <?php echo $objResult["Total"];?> บาท</td>
                </tr>
                <tr><td colspan="2"><img src="images/scb.jpg" style="width: 40px" /> SCB ไทยพาณิชย์ 996-258161-7 ออมทรัพย์ ชื่อบัญชีนาย กฤตานน เผ่าดี</td></tr>
            </tfoot>
        </table>
       <b>ส่งใบเสร็จรับเงินและติดต่อขอเลขพัสดุได้ที่</b>
       </br>1.<a href="https://www.facebook.com/MeeyerTH/"> https://www.facebook.com/MeeyerTH</a>
       </br>2.<a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a>
    </div>
</div>
 <div class="col-md-5 ">
              <center><b>รายการสินค้า</b></center>
              <div class="table-responsive" style="margin-top: 14px;">
              <table border="1" align="center" width="100%" style="border-color:#ddd;border-style: solid;">
              <thead>
                <tr style="background-color: #f8f8f8">
                <th> <center>ชื่อสินค้า</center></th>
                <th ><center>ราคาต่อหน่วย</center></th>
                <th ><center>จำนวน</center></th>
                <th><center>รวมราคา</center></th>
                </tr>
              </thead>
              <tbody>
            <?php
            $Total = 0;
            $SumTotal = 0;
            $strSQL2 = "SELECT * FROM orders_detail WHERE OrderID = '".$_GET["OrderID"]."' ";
            $objQuery2 = mysqli_query($mysqli,$strSQL2);
            while($objResult2 = mysqli_fetch_array($objQuery2))
            {
                $strSQL3 = "SELECT * FROM product WHERE ProductID = '".$objResult2["ProductID"]."' ";
                $objQuery3 = mysqli_query($mysqli,$strSQL3);
                $objResult3 = mysqli_fetch_array($objQuery3);
                $Total = $objResult2["Qty"] * $objResult3["Price"];
                $SumTotal = $SumTotal + $Total;
              ?>
                <tr align="center">
                <td align="center" ><?php echo $objResult3["ProductName"];?></td>
                <td align="center" ><?php echo $objResult3["Price"];?></td>
                <td ><?php echo $objResult2["Qty"];?></td>
                <td ><?php echo number_format($Total,2);?></td>  
                </tr>
                <?php
                }
              ?>
              </tbody>
              <tfoot>
                <tr style="background-color: #f8f8f8">
                    <td colspan="4" style="text-align: right;">
                        จำนวนเงินรวมทั้งหมด <font size="5"><b><?php echo number_format($SumTotal,2);?> </b></font>บาท
                    </td>
                </tr>
                </tfoot>
              </table>
    </div>
        <div class="clearfix"> </div>
    </div>
</div>
<div class="fandt">
    <div class="container">
        <div class="col-md-6 features">
            <h3>Our Services</h3>
            <div class="support">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-user " aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4><font size="3">ตอบกลับภายใน 24 ขั่วโมง</font></h4>
                    <p><a href="https://www.facebook.com/Meeyer-%E0%B8%9C%E0%B8%A5%E0%B8%B4%E0%B8%95%E0%B8%A0%E0%B8%B1%E0%B8%93%E0%B8%91%E0%B9%8C%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1%E0%B9%80%E0%B8%9E%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%AA%E0%B8%B8%E0%B8%82%E0%B8%A0%E0%B8%B2%E0%B8%9E-1478566848879190/?modal=admin_todo_tour"> www.facebook.com/Meeyer</a>&nbsp;
                    <br><a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="shipping">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>ขนส่งสินค้าฟรี</h4>
                    <p>บริการขนส่งสินค้าฟรีทั่วทุกจังหวัด</p>
                </div>  
                <div class="clearfix"></div>
            </div>
            <div class="money-back">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>คืนเงินเมื่อเกิดข้อผิดพลาด</h4>
                    <p>เมื่อเกิดข้อผิดพลาดระหว่างการสั่งซื้อสินค้า ทางเราจะคืนเงินให้ท่าน 100%</p>
                </div>  
                <div class="clearfix"></div>                
            </div>
        </div>
        <div class="col-md-6 testimonials">
            <div class="test-inner">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
                <script src="js/jquery.wmuSlider.js"></script> 
                                <script>
                                    $('.example1').wmuSlider();         
                                </script> 
<!-- top-brands -->
    <div class="top-brands">
        <div class="container">
        </div>
    </div>
<!-- //top-brands -->
<!-- newsletter -->
    <div class="newsletter">
        <div class="container">
            <div class="col-md-6 w3agile_newsletter_left">
                <h3>Newsletter</h3>
                <p>รับข่าวสารข้อมูลผลิตภัณฑ์ใหม่จากเราได้ที่นี่ กรอกอีเมลของท่าน</p>
            </div>
            <div class="col-md-6 w3agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Subscribe" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //newsletter -->
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
                  <a href="index.php"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
</body>
</html>