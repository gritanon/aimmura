<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="th">
<head>
<title>อาหารเสริม MazyMac แก้ปัญหาประจำเดือนมาไม่ปกติ ช่วยปรับสมดุลฮอร์โมน | Meeyer.com</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="../application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- css -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="../css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="../css/font-awesome.min.css" type="text/css" media="all" />


<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../js/bootstrap.js"></script>
<!--flex slider-->
<script defer src="../js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="../css/flexslider.css" type="text/css" media="screen" />
<script>
	// Can also be used with $(document).ready()
	$(window).load(function() {
	  $('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	  });
	});
</script>
<!--flex slider-->
<script src="../js/button-go-top.js"></script>
<script src="../js/imagezoom.js"></script>
<!-- //js --> 
<style>
    .map-responsive{
    overflow:hidden;
    padding-bottom:23%;
    position:relative;

}
.map-responsive iframe{
    left:0;
    top:0;
    height:250px;
    width:700px;
    position:absolute;
}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    width: 50px;
    height: 50px;
    border-radius: 50%; /* Rounded corners */
    opacity: 1.0;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #00e58b; /* Add a dark-grey background on hover */
    color: white; 
    opacity: 0.5;
    filter: alpha(opacity=100); 
}
</style>
</head>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน"><i class="fa fa-chevron-up"></i></button>
<?php
    session_start();
    include('setdatabase.php');
    $objDB = mysql_select_db('rewrite');
    $strSQL = "SELECT * FROM product WHERE ProductID = '".$_GET["ProductID"]."' ";
    $objQuery=mysqli_query($mysqli,$strSQL);
    $objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
?>
<div class="header-top-w3layouts">
    <div class="container">
        <div class="col-md-6 logo-w3">
            <a href="../index"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> gritanon </li>
                <li><a href="../login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-bottom-w3ls" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
    <div class="container">
        <div class="col-md-7 navigation-agileits">
            <nav class="navbar navbar-default">
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav ">
                        <li class=" active"><a href="../index" class="hyper "><span>หน้าหลัก</span></a></li> 
                        <li><a href="../contact" class="hyper"><span>ติดต่อเรา</span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
                <script>
                $(document).ready(function(){
                    $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                            $(this).toggleClass('open');        
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                            $(this).toggleClass('open');       
                        }
                    );
                });
                </script>
        <div class="col-md-4 search-agileinfo">
            <form action="" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" aria-label="Left Align">
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
            </form>
        </div>
        <div class="col-md-1 cart-wthree">
            <div class="cart"> 
                 <form action="../cart.php" method="post" class="last"> 
                    <input type="hidden" name="cmd" value="_cart" />
                    <input type="hidden" name="display" value="1" />
                    <button class="w3view-cart " type="submit" name="submit" ><i class="fa fa-cart-plus" aria-hidden="true" style="color: #00e58b;"></i></button>
                </form> 
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<style>
#myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 50px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 45%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
}

@keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
}

/* The Close Button */
.close {
    position: absolute;
    top: 20px;
    right: 35px;
    color: #f1f1f1;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
}

.close:hover,
.close:focus {
    color: #bbb;
    text-decoration: none;
    cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
    .modal-content {
        width: 100%;
    }
}
</style>
<div class="products">	 
		<div class="container">  
			<div class="single-page">
				<div class="single-page-row" id="detail-21">
					<div class="col-md-6 single-top-left">	
						<div class="flexslider">
							<ul class="slides">
			                <li data-thumb="../ViewImage1.php?ProductID=<?php echo $objResult['ProductID'];?>" >
			                  <div class="thumb-image detail_images"> <img class="" src="../ViewImage1.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="รูปสินค้า" id="myImg" title="กดเพื่อขยายรูปภาพ"> </div>
			                </li>
			                <li data-thumb="../ViewImage2.php?ProductID=<?php echo $objResult['ProductID'];?>">
			                   <div class="thumb-image detail_images"> <img class="" src="../ViewImage2.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="รูปสินค้า" id="myImg2" title="กดเพื่อขยายรูปภาพ"> </div>
			                </li>
			                <li data-thumb="../ViewImage3.php?ProductID=<?php echo $objResult['ProductID'];?>">
			                   <div class="thumb-image detail_images"> <img class="" src="../ViewImage3.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="รูปสินค้า" id="myImg3" title="กดเพื่อขยายรูปภาพ"></div>
			                </li> 
							</ul>
						</div>
					</div>
					<div class="col-md-6 single-top-right">
						<h3 class="item_name"><font color="#23282d"> <?php echo $objResult["ProductName"];?></font></h3>
						<div class="single-rating">
							<ul>
								<li><font size="3">( 1 กล่อง x 30 แคปซูล )</font></li>
							</ul> 
						</div>
						<div class="single-price">
							<ul>
			                <li><font color="red"><?php echo $objResult["Price"];?></font></li>  
			                <li><del><?php echo $objResult["discount"];?>฿</del>บาท</li> 
							</ul>	
						</div> 
						<p class="single-price-text"><?php echo $objResult["describes"];?></p>
						<form action="../order.php" method="post">
							<input type="hidden" name="txtProductID" value="<?php echo $objResult["ProductID"];?>" />
							<input class="w3ls-cart " style="width: 45px;text-align: center;background-color:white;color: black" type="text" name="txtQty" value="1"  /> 
							<button type="submit" class="w3ls-cart" ><i class="fa fa-cart-plus" aria-hidden="true" ></i>ใส่ตะกร้า</button></font>
						</form>
					</div>
				    <div class="clearfix"> </div>  
				</div>
			</div> 

<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
<div id="myModal2" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img02">
  <div id="caption"></div>
</div>
<div id="myModal3" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img03">
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[1];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg2');
var modalImg = document.getElementById("img02");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[2];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>
<script>
// Get the modal
var modal = document.getElementById('myModal3');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg3');
var modalImg = document.getElementById("img03");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[3];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}
</script>
			<!-- collapse-tabs -->
			<div class="collpse tabs">
				<h3 class="w3ls-title">เกี่ยวกับสินค้า</h3> 
				<div class="panel-group collpse" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									<i class="fa fa-file-text-o fa-icon" aria-hidden="true"></i> Description <span class="fa fa-angle-down fa-arrow" aria-hidden="true"></span> <i class="fa fa-angle-up fa-arrow" aria-hidden="true"></i>
								</a>
							</h4>
						</div>

						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" style="background-color: white;">
								<font color="#23282d"><?php echo $objResult["detail"];?></font>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
</div>
<div class="fandt">
    <div class="container">
        <div class="col-md-6 features" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25);padding-bottom: 30px;padding-top: 30px">
            <h3>Our Services</h3>
            <div class="support">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-user " aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4><font size="3">ตอบกลับภายใน 24 ขั่วโมง</font></h4>
                    <p><a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="shipping">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>ขนส่งสินค้าฟรี</h4>
                    <p>บริการขนส่งสินค้าฟรีทั่วทุกจังหวัด</p>
                </div>  
                <div class="clearfix"></div>
            </div>
            <div class="money-back">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>คืนเงินเมื่อเกิดข้อผิดพลาด</h4>
                    <p>เมื่อเกิดข้อผิดพลาดระหว่างการสั่งซื้อสินค้า ทางเราจะคืนเงินให้ท่าน 100%</p>
                </div>  
                <div class="clearfix"></div>                
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5 testimonials" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
            <div class="test-inner">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="../images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="../images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
                <script src="../js/jquery.wmuSlider.js"></script> 
                                <script>
                                    $('.example1').wmuSlider();         
                                </script> 
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
            <a href="../index"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
</body>
</html>