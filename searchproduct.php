<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin - จัดการร้านค้า</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  session_start();
  require_once("Connect.php");

  //*** Update Last Stay in Login System
  $sql = "UPDATE data_user SET LastUpdate = NOW() WHERE UserID = '".$_SESSION["UserID"]."' ";
  $query = mysqli_query($con,$sql);

  //*** Get User Login
  $strSQL = "SELECT * FROM data_user WHERE UserID = '".$_SESSION['UserID']."' ";
  $objQuery = mysqli_query($con,$strSQL);
  $objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
  if($_SESSION['UserID'] ==""){
    header("location:index.php");
  }
?>
  <header class="main-header">
    <!-- Logo -->
    <a href="admin.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">GET</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Store</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/boxed-bg.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $objResult["Name"];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $objResult["Name"];?>
                  <small><?php echo $objResult["LastUpdate"];?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $objResult["Name"];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          <a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center><font color="white">MENU</font></center></li>
        <li >
          <a href="admin.php">
            <i class="fa fa-home" aria-hidden="true"></i> <span>หน้าหลัก</span>
          </a>
        </li>
        <li>
          <a href="adorders.php">
            <i class="fa fa-list-alt" aria-hidden="true"></i> <span>รายการสั่งซื้อทั้งหมด</span>
          </a>
        </li>
        <li class="active">
          <a href="adproducts.php">
            <i class="fa fa-shopping-bag" aria-hidden="true"></i> <span>สินค้า</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php
    include('setdatabase.php');

    ini_set('display_errors', 1);
    error_reporting(~0);
    $strKeyword = null;
    if(isset($_POST["txtKeyword"]))
    {
    $strKeyword = $_POST["txtKeyword"];
    }
        $perpage = 10;
        if (isset($_GET['page'])) {
        $page = $_GET['page'];
        } else {
        $page = 1;
        }
        $start = ($page - 1) * $perpage;
    $strSQL = "SELECT  * FROM product WHERE ProductID LIKE '%".$strKeyword."%' or type LIKE '%".$strKeyword."%' or ProductName LIKE '%".$strKeyword."%' limit {$start} , {$perpage}";

    $objQuery=mysqli_query($mysqli,$strSQL);
    $Num_Rows=mysqli_num_rows($objQuery);
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        สินค้าทั้งหมด
      </h1>
      <ol class="breadcrumb">
        <li><a href="adproducts.php"><i class="fa fa-shopping-bag" aria-hidden="true"></i> สินค้า</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">ผลการค้นหา " <?php echo $strKeyword;?> "</h3>
              <div class="box-tools">
               <form name="frmSearch" method="post" action="searchproduct.php">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="txtKeyword" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive ">
              <table class="table table-hover">
                <tr>
                  <th width="80">รูปสินค้า</th>
                  <th>รหัสสินค้า</th>
                  <th>ประเภทสินค้า</th>
                  <th>ชื่อสินค้า</th>
                  <th>ราคาขายเดิม</th>
                  <th>ราคาขายใหม่</th>
                  <th></th>
                </tr>
<?php
    while($objResult = mysqli_fetch_assoc($objQuery))
  {
?>

            <tr>
                <td width="130"><img class="img-responsive" src="ViewImage.php?ProductID=<?php echo $objResult["ProductID"];?>"> </td>
                <td> <?php echo $objResult["ProductID"];?></td>
                <td> <?php echo $objResult["type"];?></td>
                <td> <?php echo $objResult["ProductName"];?></td>
                <td> <?php echo $objResult["Price"];?></td>
                <td> <?php echo $objResult["discount"];?></td>
                <td><a href="item.php?ProductID=<?php echo $objResult["ProductID"];?>">
                <button type="button" class="btn btn-primary"> 
                ดูรายละเอียด</button></a></td>
            </tr>
<?php
}
?>  
              </table>
<?php
$strSQL2="SELECT*FROM product ORDER BY ProductID DESC";
$objQuery2=mysqli_query($mysqli,$strSQL2);
$total_record = mysqli_num_rows($objQuery2);
$total_page = ceil($total_record / $perpage);
?>
         <nav>
           <center>
             <ul class="pagination">
               <li>
                 <a href="adproducts.php?page=1" aria-label="Previous">
                 <span aria-hidden="true">&laquo;</span>
                 </a>
                 </li>
                 <?php for($i=1;$i<=$total_page;$i++){ ?>
                 <li><a href="adproducts.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                 <?php } ?>
                 <li>
                 <a href="adproducts.php?page=<?php echo $total_page;?>" aria-label="Next">
                 <span aria-hidden="true">&raquo;</span>
                 </a>
               </li>
             </ul>
           </center>
         </nav>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 15px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #1e282c; /* Set a background color */
    color: #b8c7ce; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
    width: 45px;
    opacity: 0.5;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #222d32; /* Add a dark-grey background on hover */
    color: white;  
    opacity: 1.0;
    filter: alpha(opacity=100);
}
</style>
<button onclick="topFunction()" id="myBtn"><i class="fa fa-chevron-up"></i></button>
</body>
</html>