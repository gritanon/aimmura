<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin - จัดการร้านค้า</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php
  session_start();
  require_once("Connect.php");

  //*** Update Last Stay in Login System
  $strSQL = "UPDATE data_user SET LastUpdate = NOW() WHERE UserID = '".$_SESSION["UserID"]."' ";
  $objQuery=mysqli_query($mysqli,$strSQL);  

  //*** Get User Login
  $strSQL = "SELECT * FROM data_user WHERE UserID = '".$_SESSION['UserID']."' ";
  $objQuery=mysqli_query($mysqli,$strSQL);  
  $objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
  if($_SESSION['UserID'] ==""){
    header("location:index.php");
  }
?>
  <header class="main-header">
    <!-- Logo -->
    <a href="admin.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">GET</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Store</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/boxed-bg.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $objResult["Name"];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $objResult["Name"];?>
                  <small><?php echo $objResult["LastUpdate"];?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/boxed-bg.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $objResult["Name"];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          <a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><center><font color="white">MENU</font></center></li>
        <li >
          <a href="admin.php">
            <i class="fa fa-home" aria-hidden="true"></i> <span>หน้าหลัก</span>
          </a>
        </li>
        <li>
          <a href="adorders.php">
            <i class="fa fa-list-alt" aria-hidden="true"></i> <span>รายการสั่งซื้อทั้งหมด</span>
          </a>
        </li>
        <li>
          <a href="adtype.php">
            <i class="fa fa-server" aria-hidden="true"></i><span>ประเภทสินค้า</span>
          </a>
        </li>
        <li>
          <a href="adproducts.php">
            <i class="fa fa-shopping-bag" aria-hidden="true"></i> <span>สินค้า</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<?php 
  $strSQL = "SELECT * FROM product WHERE ProductID = '".$_GET["ProductID"]."' ";
  $objQuery=mysqli_query($mysqli,$strSQL);  
	$objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        แก้ไขข้อมูลสินค้า
      </h1>
      <ol class="breadcrumb">
        <li><a href="adproducts.php"><i class="fa fa-shopping-bag" aria-hidden="true"></i> สินค้า</a></li>
        <li class="active">รหัสสินค้า <?php echo $objResult["ProductID"];?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row ">
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $objResult["ProductName"];?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="post">
                    <div class="col-sm-12">
                      <p><b>รูปแสดงด้านในรายละเอียด</b></p>
                      <table class="table">
                        <tr>
                            <td>
                              <form action="UploadToSQimg1.php?ProductID=<?php echo $objResult['ProductID'];?>" method="post" enctype="multipart/form-data">
                              <img class="img-responsive" width="140" src="ViewImage1.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
                              <p><input type="file" name="Picture1"></p>
                              <button type="submit" class="btn btn-primary btn-xs">เพิ่ม</button>
                              </form>
                            </td>
                            <td>
                              <form action="UploadToSQimg2.php?ProductID=<?php echo $objResult['ProductID'];?>" method="post" enctype="multipart/form-data">
                              <img class="img-responsive" width="140" src="ViewImage2.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
                              <p><input type="file" name="Picture2"></p>
                              <button type="submit" class="btn btn-primary btn-xs">เพิ่ม</button>
                              </form>
                            </td>
                            <td>
                              <form action="UploadToSQimg3.php?ProductID=<?php echo $objResult['ProductID'];?>" method="post" enctype="multipart/form-data">
                              <img class="img-responsive" width="140" src="ViewImage3.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
                              <p><input type="file" name="Picture3"></p>
                              <button type="submit" class="btn btn-primary btn-xs">เพิ่ม</button>
                              </form>
                            </td>
                            <td>
                              <form action="UploadToSQimg4.php?ProductID=<?php echo $objResult['ProductID'];?>" method="post" enctype="multipart/form-data">
                              <img class="img-responsive" width="140" src="ViewImage4.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
                              <p><input type="file" name="Picture4"></p>
                               <button type="submit" class="btn btn-primary btn-xs">เพิ่ม</button>
                              </form>
                            </td>
                        </tr>
                      </table>
                      <!-- /.row -->
                    </div>
                  <!-- /.user-block -->
                  <div>
                  <form action="UploadToSQLedit.php?ProductID=<?php echo $objResult['ProductID'];?>" method="post" enctype="multipart/form-data">
                    <div class="col-sm-12">
                      <center>
                      <img class="img-responsive" width="140" src="ViewImage.php?ProductID=<?php echo $objResult['ProductID'];?>">
                      <p><b>เลือกรูปภาพ แสดงกับสินค้าอื่น</b><input type="file" name="filUpload"></p>
                      </center>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>รหัสสินค้า</label>
                        <input type="text" name="id" value="<?php echo $objResult['ProductID'];?>" class="form-control" >
                      </div>
                      <div class="form-group">
                        <label>ชื่อสินค้า</label>
                        <input type="text" name="name" value="<?php echo $objResult['ProductName']; ?>" class="form-control" >
                      </div>
                      <div class="form-group">
                        <label>ราคาขายเดิม</label>
                        <input type="text" name="discount" value="<?php echo $objResult['discount'];?>" class="form-control" >
                      </div>
                      <div class="form-group">
                        <label>ราคาขายใหม่</label>
                        <input type="text" name="price" value="<?php echo $objResult['Price'];?>" class="form-control" >
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>ประเภทสินค้า</label>
                        <input type="text" name="type" value="<?php echo $objResult['type'];?>" class="form-control" >
                      </div>
                      <div class="form-group">
                        <label>คำอธิบายสินค้า</label>
                        <textarea class="form-control" name="describes" rows="9" ><?php echo $objResult["describes"];?></textarea>
                      </div>
                    </div>
                      <script src="ckeditor.js"></script>
                    <div class="col-sm-12">
                        <label>รายละเอียดสินค้า</label>
                        <textarea id="detail" name="detail" >
                            <?php echo $objResult["detail"];?>
                        </textarea>

                        <script>
                          CKEDITOR.replace( 'detail');
                        </script>
                    </div>
                    <div class="col-sm-12">
                        <p align="right"><button type="submit" class="btn btn-primary btn-xs">บันทึก</button>
                        </form>
                        <a href="adproducts.php"><button type="submit" class="btn btn-danger btn-xs">กลับ</button></p></a>
                    </div>
                    </div>
                    <!-- /.col -->
                  </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 15px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #1e282c; /* Set a background color */
    color: #b8c7ce; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
    width: 45px;
    opacity: 0.5;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #222d32; /* Add a dark-grey background on hover */
    color: white;  
    opacity: 1.0;
    filter: alpha(opacity=100);
}
</style>
<button onclick="topFunction()" id="myBtn"><i class="fa fa-chevron-up"></i></button>
</body>
</html>