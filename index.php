<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="th">
<head>
<title>Aimmura เอมมูร่า เซซามิน อาหารเสริมเพื่อสุขภาพ งานวิจัยมหาวิทยาลัยเชียงใหม่</title>
<meta name="description" content="เอมมูร่า เซซามิน Aimmura Sesamin สารสกัดงาดำ อาหารเสริมเพื่อสุขภาพ ยับยั้งเซลล์มะเร็ง ลดอาการอักเสบของกระดูกพรุน ข้อเสื่อม อัมพาต อัมพฤกษ์ โรคหลอดเลือดสมอง จากงานวิจัย ม.ช. จดสิทธิบัตรทั่วโลก จัดส่งฟรี EMS Line ID : Gritanon
โทร : 096-1515236">
<meta name="keywords" content="อาหารเสริมเพื่อสุขภาพ,อาหารเสริม,งานวิจัยมหาวิทยาลัยเชียงใหม่,มะเร็ง,ข้อเสื่อม,sesamin,รักษาโรค,aimmura,Aimmurax,Aimmurao,Aimmurae,Aimmurav,เซซามิน,งาดำ,เอมมูร่า,เอมมูร่าเซซามิน,ไอยรา,ผลิตภัณฑ์ไอยรา">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
<link rel="shortcut icon" href="favicon.ico">
<!--// css -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/button-go-top.js"></script>
<style>
    .map-responsive{
    overflow:hidden;
    padding-bottom:23%;
    position:relative;

}
.map-responsive iframe{
    left:0;
    top:0;
    height:250px;
    width:700px;
    position:absolute;
}
</style>
<style>
    #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #00e58b; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 10px; /* Some padding */
    width: 50px;
    height: 50px;
    border-radius: 50%; /* Rounded corners */
    opacity: 1.0;
    filter: alpha(opacity=40);
}

#myBtn:hover {
    background-color: #00e58b; /* Add a dark-grey background on hover */
    color: white; 
    opacity: 0.5;
    filter: alpha(opacity=100); 
}
</style>
</head>
<?php
session_start();
if(@$_SESSION["lang"] == "EN")
{
  include("en.php");
}
else
{
  include("th.php");
}
?>
<body>
<button onclick="topFunction()" id="myBtn" title="คลิ๊ก ไปด้านบน"><i class="fa fa-chevron-up"></i></button>
<?php
        include('setdatabase.php');
        $objDB = mysql_select_db('rewrite');
        
        $perpage = 8;
        if (isset($_GET['page'])) {
        $page = $_GET['page'];
        } else {
        $page = 1;
        }

        $start = ($page - 1) * $perpage;

        $strSQL = "SELECT * FROM product limit {$start} , {$perpage} ";
        $objQuery=mysqli_query($mysqli,$strSQL);
?>
<div class="header-top-w3layouts" >
    <div class="container">
        <div class="col-md-6 logo-w3">
             <a href="index"><h1><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></h1></a>
        </div>
        <div class="col-md-6 phone-w3l">
            <ul>
                <li><a href="http://line.me/ti/p/bwa_7Ua1RJ"><font color="#337ab7"><b>LINE ID</b></font></a> gritanon </li>
                <li><a href="login.php"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></li>
                <li>096-1515236</li>
                <li><a href="change.php?lang=TH">TH</a> , <a href="change.php?lang=EN">EN</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="header-bottom-w3ls" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
    <div class="container">
        <div class="col-md-7 navigation-agileits">
            <nav class="navbar navbar-default">
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav ">
                        <li class=" active"><a href="index" class="hyper "><span><?php echo $strHome;?></span></a></li> 
                        <li><a href="contact" class="hyper"><span><?php echo $strContact;?></span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
                                <script>
                $(document).ready(function(){
                    $(".dropdown").hover(            
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                            $(this).toggleClass('open');        
                        },
                        function() {
                            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                            $(this).toggleClass('open');       
                        }
                    );
                });
                </script>
        <div class="col-md-4 search-agileinfo">
            <form action="" method="post">
                <input type="search" name="Search" placeholder="Search for a Product..." required="">
                <button type="submit" class="btn btn-default search" aria-label="Left Align">
                    <i class="fa fa-search" aria-hidden="true"> </i>
                </button>
            </form>
        </div>
        <div class="col-md-1 cart-wthree">
            <div class="cart"> 
                 <form action="cart.php" method="post" class="last"> 
                    <input type="hidden" name="cmd" value="_cart" />
                    <input type="hidden" name="display" value="1" />
                    <button class="w3view-cart " type="submit" name="submit" ><i class="fa fa-cart-plus" aria-hidden="true" style="color: #00e58b;"></i></button>
                </form> 
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="banner" style="padding-top: 15px">
    <div class="container">
            <div class="col-md-6 w3agile_newsletter_right" >
                <div class="embed-responsive  rounded embed-responsive-16by9" >
                <iframe class="embed-responsive-item rounded" src="https://www.youtube.com/embed/rQTGKLNNWus?autoplay=1" allowfullscreen=""> </iframe>
                </div>
            </div>
            <div class="col-md-6 w3agile_newsletter_left">
                <br>
                <font color="black"><h2><b><a href="index"><h1><font color="#333232" style="font-weight: bolder;">MEEYER </font>
                    <font color="#e5b822" style="font-weight: bolder;">.COM</font></h1></a></b></h2></br>
                <h3><?php echo $strHeader;?></h3></font></br>
                  <img src="/allproduct.png" alt=" " class="img-responsive" />
            </div>

            <div class="clearfix"> </div>
    </div>
</div>
<div class="top-products">
    <div class="container">
<?php while ($objResult = mysqli_fetch_assoc($objQuery)) { ?>
      <form action="order.php" method="post">
            <div class="col-md-3 top-product-grids">
            <a href="<?php echo $objResult["ProductID"];?>/<?php echo $objResult["ProductName"];?>" title="คลิ๊ก อ่านข้อมูลสินค้า">
            <div class="product-img" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
                 <img class="img-responsive" width="140" src="ViewImage.php?ProductID=<?php echo $objResult['ProductID'];?>" alt="Photo">
                <div class="p-mask">
                    <form action="#" method="post">
                    <input type="hidden" name="txtQty" value="1" /> 
                    <input type="hidden" name="txtProductID" value="<?php echo $objResult["ProductID"];?>" size="2">
                    <button type="submit" class="w3ls-cart pw3ls-cart" title="คลิ๊ก ใส่ตะกร้า"><font color="#5e5f5f"><center><b>Add to cart / ใส่ตะกร้า </b></center></font></button>
                    </form>
                </div>
            </div></a>
            <h4> <a href="product.php?ProductID=<?php echo $objResult["ProductID"];?>" title="<?php echo $strRead;?>"><?php echo $objResult["ProductName"];?></a></h4>
            <h5><?php echo $objResult["Price"];?> <span><?php echo $strBaht;?> </h5>
            <br><br>                    
            </div>          
      </form>
<?php } ?>
    <div class="clearfix"></div>
    </div>
    <center>
    <?php
     $strSQL2 = "SELECT * FROM product ";
     $objQuery2=mysqli_query($mysqli,$strSQL2);
     $total_record = mysqli_num_rows($objQuery2);
     $total_page = ceil($total_record / $perpage);
     ?>
    <nav>
     <ul class="pagination">
         <li>
             <a href="index.php?page=1" aria-label="Previous">
             <span aria-hidden="true">&laquo;</span>
             </a>
         </li>
             <?php for($i=1;$i<=$total_page;$i++){ ?>
             <li><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
         <?php } ?>
         <li>
             <a href="index.php?page=<?php echo $total_page;?>" aria-label="Next">
             <span aria-hidden="true">&raquo;</span>
             </a>
         </li>
     </ul>
     </nav>
    </center>
</div>                       
    <script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });     
    </script>

    <hr>
    <div >
        <div class="col-md-6 col-md-offset-3" style="border: 1px;border-style: solid;">
                <h1 style="padding-top: 5px">
                    AIMMURA SESAMIN
                </h1>
                
                <p style="padding-bottom: 25px"></br>
                    “ตะลึง” เซซามิน การค้นพบครั้งแรกของโลก..มหัศจรรย์สารเซซามิน...สารสกัดจากงาดำ...มีฤทธิ์ต้านมะเร็งได้
</br>✅ช่อง3 ข่าววันใหม่ นักวิจัย ม เชียงใหม่ ค้นพบสารรักษามะเร็งจากงาดำครั้งแรกของโลกคลิกดูข่าว 👉https://youtu.be/sVVzFC7KDLk
</br>
ศ.ดร.ปรัชญา คงทวีเลิศ ดังอีกแล้ว ... ดังต่อเนื่อง !!! 
คราวนี้ ในช่อง #TNN24 ชมที่ลิงค์นี้นะคะ ช่วยกันแชร์ต่อๆกันด้วยค่ะ
เจาะลึก!! ว่าทำไม? "สารเซซามินในงาดำ" ถึงรักษามะเร็งได้
✅https://youtu.be/tNalU7tioiU</br>
</br>
ประโยชน์และสรรพคุณของงาดำ รวมถึงขั้นตอนการสกัดสารเซซามินจากงาดำ
👉https://youtu.be/5kO3_aHObBw</br>
</br>
✨ถือเป็นข่าวดีในวงการวิจัยและวงการแพทย์ของไทย และเป็นอีกหนึ่งความหวังของผู้ป่วย เมื่อนักวิจัยของภาควิชาชีวเคมี คณะแพทยศาสตร์มหาวิทยาลัยเชียงใหม่ ได้วิจัยและค้นพบว่าใน “งาดำ” มีสารเซซามิน ที่ช่วยยับยั้งเซลล์มะเร็ง และช่วยฟื้นฟูเซลล์สมองที่เสื่อมจากการเจ็บป่วย หรือ ได้รับการกระทบกระเทือนจากอุบัติเหตุได้เป็นครั้งแรกของโลก✨</br>
</br>
🏆ผลิตภัณฑ์เสริมอาหารAimmura เอมมูร่า ผลงานวิจัย ศ.ดร.ปรัชญา คงทวีเลิศ</br>
- มีงานวิจัยรองรับ</br>
- มีสถาบันวิจัยชัดเจน</br>
- มีนักวิจัยที่เป็นตัวจริงทางด้านงานวิจัย</br>
- มีสิทธิบัตรคุ้มครองของระดับโลกและที่สำคัญเป็นของคนไทย 100%</br>
-จดสิทธิบัตรแล้ว 3 ประเทศ ได้แก่ ไทย USA มาเลเซีย</br>
</br>
"เสียเงินบาทเพื่อป้องกัน ดีกว่าเสียเงินพันเพื่อรักษา"</br>
ขนาดบรรจุ 60 แคปซูล/ขวด</br>
วิธีทาน รับประทานครั้งละ 2 แคปซูล วันละ 2 ครั้ง (เช้า-เย็น) (ก่อนรับประทานอาหารประมาณ 30 นาที)</br>
รายละเอียดเพิ่มเติมคลิกเลย</br>
👉http://www.meeyer.com</br>
</br>
สั่งซื้อโทรเลย 📲096-1515-236
✅Line id: gritanon (สั่งซื้อสินค้า/แจ้งโอนเงิน)
✅Facebook : Meeyer (สั่งซื้อสินค้า/แจ้งโอนเงิน)
                </p>
        </div>
    </div>
    <hr style="clear: both;">
    <div class="mbr-arrow hidden-sm-down" aria-hidden="true">
        <a href="#next">
            <i class="mbri-down mbr-iconfont"></i>
        </a>
    </div>
</section>
<div class="fandt">
    <div class="container">
        <div class="col-md-6 features" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25);padding-bottom: 30px;padding-top: 30px">
            <h3>Our Services</h3>
            <div class="support">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-user " aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4><font size="3">ตอบกลับภายใน 24 ขั่วโมง</font></h4>
                    <p><a href="http://line.me/ti/p/bwa_7Ua1RJ"> http://line.me/ti/p/bwa_7Ua1RJ</a></p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="shipping">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-bus" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>ขนส่งสินค้าฟรี</h4>
                    <p>บริการขนส่งสินค้าฟรีทั่วทุกจังหวัด</p>
                </div>  
                <div class="clearfix"></div>
            </div>
            <div class="money-back">
                <div class="col-md-2 ficon hvr-rectangle-out">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                <div class="col-md-10 ftext">
                    <h4>คืนเงินเมื่อเกิดข้อผิดพลาด</h4>
                    <p>เมื่อเกิดข้อผิดพลาดระหว่างการสั่งซื้อสินค้า ทางเราจะคืนเงินให้ท่าน 100%</p>
                </div>  
                <div class="clearfix"></div>                
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-5 testimonials" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
            <div class="test-inner">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;"> 
                            <div class="banner-wrap">
                                <img src="images/me.png" alt=" " class="img-responsive" />
                                <p>ผู้จำหน่ายสินค้า และ ผู้พัฒนาเว็บไซต์ <br>ตัวแทนจำหน่ายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพ AIYARA</p>
                                <h4># กฤตานน เผ่าดี</h4>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
                <script src="js/jquery.wmuSlider.js"></script> 
                                <script>
                                    $('.example1').wmuSlider();         
                                </script> 
<!-- top-brands -->
    <div class="top-brands">
        <div class="container">
        </div>
    </div>
<!-- //top-brands -->
<!-- newsletter -->
    <div class="newsletter" style="box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.25)">
        <div class="container">
            <div class="col-md-6 w3agile_newsletter_left">
                <h3>Newsletter</h3>
                <p>รับข่าวสารข้อมูลผลิตภัณฑ์ใหม่จากเราได้ที่นี่ กรอกอีเมลของท่าน</p>
            </div>
            <div class="col-md-6 w3agile_newsletter_right">
                <form action="#" method="post">
                    <input type="email" name="Email" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <input type="submit" value="Subscribe" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
<!-- //newsletter -->
<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-grids fgd1">
            <a href="index"><font color="#333232" style="height: 5px;font-weight: bolder;">MEEYER </font>
            <font color="#e5b822" style="height: 5px;font-weight: bolder;">.COM</font></a>
        <ul>
            <li>287/1 หมู่ 2 ต.เหมืองง่า</li>
            <li>จังหวัดลำพูน 51000</li>
            <li><a href="mailto:info@example.com">tanyarataimmura@gmail.com</a></li>
            <li>LINE ID:gritanon, Facebook:Meeyer</li>
            <li>โทร 096-1515236 (เก็ท)</li>
        </ul>
        </div>
        <div class="col-md-8 footer-grids fgd1 map-responsive"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.041983517707!2d99.00492351431991!3d18.57214587244337!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30dbcd5c20d063a1%3A0x86b8dde623db4ce8!2zVE9LT1NIT1Ag4Lij4LmJ4Liy4LiZ4LmC4LiV4LmC4LiBIOC4peC4s-C4nuC4ueC4mQ!5e0!3m2!1sth!2sth!4v1509161660689" frameborder="0" style="border:0;" allowfullscreen></iframe></div>
        <div class="clearfix"></div>
        <p class="copy-right">Copyright © 2016 Meeyer.com | เว็บขายผลิตภัณฑ์อาหารเสริมออนไลน์</p>
    </div>
</div>
<div style="position: fixed; z-index: 2147483642; font-size: 0px; left: 10px; bottom: 13px;"><a href="http://line.me/ti/p/bwa_7Ua1RJ"><img class="radius" src="lines.png" alt="chat button" style="border: none;"></a></div>
</body>
</html>